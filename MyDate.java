public class MyDate {

    private int day, month, year;


    int[] days = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month - 1;
        this.year = year;
    }

    public String toString() {
        return this.year + "-" + this.month + "-" + this.day;
    }

    public boolean leapYear() {
        return year % 4 == 0;
    }

    public void incrementDay() {
        if ((this.day + 1) > days[month - 1]) {
            if (this.month == 2 && leapYear()) {
                this.day = 29;
            } else {
                this.day = 1;
                this.month += 1;
            }
        } else {
            this.day += 1;
        }
    }

    public void incrementDay(int DayPlus) {
        if ((this.day + DayPlus) > days[month - 1]) {
            this.day += (DayPlus - (days[month - 1] - this.day));
            this.month += 1;
        } else {
            this.day += DayPlus;
        }
    }

    public void decrementDay() {
        if (this.day == 1) {
            this.month -= 1;
            if (this.month == 2 && leapYear()) {
                this.day = 29;
            } else {
                this.day = days[month - 1];
            }
        } else {
            this.day -= 1;
        }
    }

    public void decrementDay(int dayMinus) {
        if (this.day - dayMinus <= 0) {
            this.month -= 1;
            this.day -= dayMinus;
            if (this.day == 0) {
                this.day = days[month - 1];
            } else {
                this.day = this.day + days[month - 1];
            }

        } else {
            this.day -= dayMinus;
        }

    }

    public void incrementMonth() {
        if (this.month == 12) {
            this.year += 1;
            this.month = 1;
        } else {
            this.month += 1;
        }
    }

    public void incrementMonth(int MonthPlus) {
        if (this.month + MonthPlus > 12) {
            this.year += (this.month + MonthPlus) % 12;
            this.month = (this.month + MonthPlus) - (((this.month + MonthPlus) % 12) * 12);
        } else {
            this.month += MonthPlus;
        }
        if (this.day > days[month - 1]) {
            if (this.month == 2 && leapYear()) {
                this.day = 29;
            } else {
                this.day = days[month - 1];
            }
        }
    }

    public void decrementMonth() {
        if (this.month == 1) {
            this.year -= 1;
            this.month = 12;
        } else {
            this.month -= 1;
        }
    }

    public void decrementMonth(int monthMinus) {
        this.month -= monthMinus;
        if (this.day <= 0) {
            this.day = days[month - 1] + this.day;
            this.month -= 1;
            while (this.day <= 0) {
                this.day = days[month - 1] + this.day;
                this.month -= 1;
            }
        }
        if (this.month <= 0) {
            this.month = 12 + this.month;
            this.year -= 1;
            while (this.month <= 0) {
                this.month = 12 + this.month;
                this.year -= 1;

            }
        }
        if (this.day > days[month - 1]) {
            this.day -= 1;
        }

    }

    public void incrementYear() {
        this.year += 1;
    }

    public void incrementYear(int yearPlus) {
        this.year += yearPlus;
    }

    public void decrementYear() {
        if (leapYear() && this.month == 2) {
            this.year -= 1;
            this.day -= 1;
        } else {
            this.year -= 1;
        }

    }

    public void decrementYear(int yearMinus) {
        this.year -= yearMinus;
    }

    public boolean isBefore(MyDate anotherDate) {
        if (this.year > anotherDate.year) {
            return false;
        }
        if (this.year == anotherDate.year && this.month > anotherDate.month) {
            return false;
        }
        if (this.year == anotherDate.year && this.month == anotherDate.month && this.day > anotherDate.day) {
            return false;
        }
        if (this.year == anotherDate.year && this.month == anotherDate.month && this.day == anotherDate.day) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isAfter(MyDate anotherDate) {
        if (this.year > anotherDate.year) {
            return true;
        }
        if (this.year == anotherDate.year && this.month > anotherDate.month) {
            return true;
        }
        if (this.year == anotherDate.year && this.month > anotherDate.month && this.day > anotherDate.day) {
            return true;
        } else {
            return false;
        }
    }

    public int dayDifference(MyDate anotherDate) {
        int comingFromYears = (this.year - anotherDate.year) * 365;
        int comingFromMonths = (this.month - anotherDate.month) * 30;
        int comingFromDays = (this.day - anotherDate.day);
        return Math.abs(comingFromDays + comingFromMonths + comingFromYears);
    }
}